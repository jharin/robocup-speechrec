#!/bin/sh
set -e

BASEDIR=$(dirname $(readlink -f $0))
cd ${BASEDIR}

if [ ! -n "$prefix" ]; then
        echo "Environment variable \"\$prefix\" must be set!"
        exit 1
fi

mkdir -p ${prefix}/share/SpeechRec/psGrammar

for i in `ls` 
do
  if [ -d $i ]
    then cp -r $i ${prefix}/share/SpeechRec/psGrammar
  fi
done

echo "copying done"
